# Stopky
![stopky vyrobené pomocí STM-8 a displeje MAX7219.](pic/stopky2.jpg)

Stopky vyrobené pomocí STM-8 a displeje MAX7219.

## Funkce programu

Stopky se spínají pomocí user tlačítka na STM8. Při prvním zmáčknutí se stopky spustí, při druhém zmáčknutí se zasztaví a při třetím se resetují. Čas je zobrazen na osmi-místém sedmisegmentový displeji.

## Kicad schema
![schema](pic/kicad schema.PNG)

## MAX7219

Jedná se o sedmisegmentový displej s osmi znaky. Používá řadič MAX7219, díky kterému pro připojení komunikace s mikrokontrolerem stačí 3 vodiče (DIN, CS, CLK). Pro komunikaci je možné používat logické úrovně 3.3V a 5V. Díky MAX7219 je možné kaskádové zapojenení více stejných displejů.Model řadiče, který byl použit, je v SMD pouzdře. MAX7219 dokáže obsluhovat až 64 individuálních LED, tzn. až 8 sedmisegmentových cifer. Řadič obsahuje 64-bitovou paměť pro uložení nastavení rozsvícených LED, tudíž není nutné průběžné programování každý programový cyklus, ale pouze při změně.

![Osmimístný sedmisegmentový displej s MAX7219](pic/3182.jpg)

## Použité součástky
|   Typ součástky |      Počet kusů      |  Cena/1 ks  | Cena  |
|:---------------:|:--------------------:|:-----------:|:-----:|
| STM8 Nucleo     |    1                 | 250 Kč      | 250 Kč|
| MAX7219         |    1                 |  47 Kč      | 47 Kč |

odkaz: [7-segmentový displej s budičem MAX7219](https://dratek.cz/arduino/3182-led-displej-7-segmentovy-8-znaku-max7219-cerveny.html?addToFavorit)

## Blokové schéma

```mermaid
flowchart TD
PWR[Napájení PC] ==> MCU[STM8]
User_tlačítko ==> MCU[STM8]
MCU==>Displej

```
