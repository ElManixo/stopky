#include "stm8s.h"
#include "custom.h"

// makra kterými volíme komunikační piny
#define CLK_GPIO GPIOD				// port na kterém je CLK vstup budiče
#define CLK_PIN  GPIO_PIN_4		// pin na kterém je CLK vstup budiče
#define DATA_GPIO GPIOD				// port na kterém je DIN vstup budiče
#define DATA_PIN  GPIO_PIN_3	// pin na kterém je DIN vstup budiče
#define CS_GPIO GPIOD					// port na kterém je LOAD/CS vstup budiče
#define CS_PIN  GPIO_PIN_2		// pin na kterém je LOAD/CS vstup budiče

// makra která zpřehledňují zdrojový kód a dělají ho snáze přenositelným na jiné mikrokontroléry a platformy
#define CLK_HIGH 			GPIO_WriteHigh(CLK_GPIO, CLK_PIN)
#define CLK_LOW 			GPIO_WriteLow(CLK_GPIO, CLK_PIN)
#define DATA_HIGH 		GPIO_WriteHigh(DATA_GPIO, DATA_PIN)
#define DATA_LOW 			GPIO_WriteLow(DATA_GPIO, DATA_PIN)
#define CS_HIGH 			GPIO_WriteHigh(CS_GPIO, CS_PIN)
#define CS_LOW 				GPIO_WriteLow(CS_GPIO, CS_PIN)

// makra adres/příkazů pro čitelnější ovládání MAX7219
#define NOOP 					0  	// No operation
#define DIGIT0 				1		// zápis hodnoty na 1. cifru
#define DIGIT1 				2		// zápis hodnoty na 1. cifru
#define DIGIT2 				3		// zápis hodnoty na 1. cifru
#define DIGIT3 				4		// zápis hodnoty na 1. cifru
#define DIGIT4 				5		// zápis hodnoty na 1. cifru
#define DIGIT5 				6		// zápis hodnoty na 1. cifru
#define DIGIT6 				7		// zápis hodnoty na 1. cifru
#define DIGIT7 				8		// zápis hodnoty na 1. cifru
#define DECODE_MODE 	9		// Aktivace/Deaktivace znakové sady (my volíme vždy hodnotu DECODE_ALL)
#define INTENSITY 		10	// Nastavení jasu - argument je číslo 0 až 15 (větší číslo větší jas)
#define SCAN_LIMIT 		11	// Volba počtu cifer (velikosti displeje) - argument je číslo 0 až 7 (my dáváme vždy 7)
#define SHUTDOWN 			12	// Aktivace/Deaktivace displeje (ON / OFF)
#define DISPLAY_TEST 	15	// Aktivace/Deaktivace "testu" (rozsvítí všechny segmenty)

// makra argumentů
// argumenty pro SHUTDOWN
#define DISPLAY_ON		1		// zapne displej
#define DISPLAY_OFF		0		// vypne displej
// argumenty pro DISPLAY_TEST
#define DISPLAY_TEST_ON 	1	// zapne test displeje
#define DISPLAY_TEST_OFF 	0	// vypne test displeje
// argumenty pro DECODE_MOD
#define DECODE_ALL			0b11111111 // (lepší zápis 0xff) zapíná znakovou sadu pro všechny cifry
#define DECODE_NONE			0 // vypíná znakovou sadu pro všechny cifry

uint8_t flag=0;
uint32_t cas;
uint32_t vterina;
uint32_t sekundy;
uint32_t minuta;
uint32_t minut;
uint32_t alpha;

void max7219_init(void);
void max7219_posli(uint8_t adresa, uint8_t data);

void zapsat(void)
{   

    max7219_posli(DIGIT0,vterina);
    max7219_posli(DIGIT1,sekundy);
    max7219_posli(DIGIT2,0b11111111);
    max7219_posli(DIGIT3,minuta);
    max7219_posli(DIGIT4,minut);
    max7219_posli(DIGIT5,0b11111111);
    max7219_posli(DIGIT6,0);
    max7219_posli(DIGIT7,0);
}

void prepocet(void)
{
    sekundy = cas % 60;
    vterina = sekundy % 10;
    sekundy = sekundy - vterina;
    sekundy = sekundy / 10;
    alpha = cas - vterina - 10*sekundy;
    alpha = alpha / 60;
    minuta = alpha % 10;
    alpha = alpha - minuta;
    minut = alpha/10; 
    if(minut == 10)
    {
        cas == 0;
        prepocet();
    }
    zapsat();
}

INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
    
    flag = flag + 1;
    if (flag == 3)
        {flag = 0;}
    for(uint32_t i ; i < 1000 ; i++)
        {
        ;
        }
}


void main(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    max7219_init();
    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);

    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);

    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_ONLY);
    ITC_SetSoftwarePriority(ITC_IRQ_PORTE, ITC_PRIORITYLEVEL_0);
    enableInterrupts();
    
    delay_s_init();

    
    while (1)
    {   if(flag == 0)
        {
            delay_s(1);
            cas = cas + 1;
            GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
            prepocet();
        }
        else if(flag == 1)
        {
            ;
        }
        else if(flag == 2)
        {
            cas = 0;
            prepocet();
            TIM3_ClearFlag (TIM3_FLAG_UPDATE);
        }       
    }
 
}

void max7219_init(void){
GPIO_Init(CS_GPIO,CS_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);
GPIO_Init(CLK_GPIO,CLK_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);
GPIO_Init(DATA_GPIO,DATA_PIN,GPIO_MODE_OUT_PP_LOW_SLOW);

max7219_posli(DECODE_MODE, DECODE_ALL);
max7219_posli(SCAN_LIMIT, 7);
max7219_posli(INTENSITY, 5);
max7219_posli(DISPLAY_TEST, DISPLAY_TEST_OFF);
max7219_posli(SHUTDOWN, DISPLAY_ON);
}

void max7219_posli(uint8_t adresa, uint8_t data){
uint8_t maska;
CS_LOW;

maska = 0b10000000;
CLK_LOW;
while(maska){
	if(maska & adresa){
		DATA_HIGH;
	}
	else{
		DATA_LOW;
	}
	CLK_HIGH;
	maska = maska>>1;
	CLK_LOW;
}

maska = 0b10000000;
while(maska){
	if(maska & data){
		DATA_HIGH;
	}
	else{ 
		DATA_LOW;
	}
	CLK_HIGH;
	maska = maska>>1;
	CLK_LOW;
}
CS_HIGH;
}